
import React, {useState, useEffect, useMemo}  from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './components/Homescreen';
import Login from './components/Login';
import Signup from './components/Signup';
import {AuthContext} from './components/context'
import { not } from 'react-native-reanimated';



const Stack = createStackNavigator();

function App() {
  const [isLoading, setisLoading] = useState(true);
  const [userToken, setuserToken] = useState(null);

  // const authContext = useMemo(() => ({
  //   signIn : () => {
  //     setuserToken('fghk');
  //     setisLoading(false);
  //   },
  //   signOut : () => {
  //     setuserToken(null);
  //     setisLoading(false);
  //   },
  //   signUp : () => {
  //     setuserToken('fghk');
  //     setisLoading(false);
  //   },
  // }))

  useEffect(() => {
   
    setTimeout(() => {
      setisLoading(false);
    }, 1000);
   
  }, [])

  if(isLoading){
    return(
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator  size="large"/>
      </View>
    );
  }
  return (
    
    <NavigationContainer>
      
      <Stack.Navigator>
      <Stack.Screen name="Login" component={Login}  options={{ title : '', headerShown:false }}/> 
      <Stack.Screen name="Home" component={HomeScreen} />  
      <Stack.Screen name="Signup" component={Signup} />
      </Stack.Navigator>
      
    </NavigationContainer>
    
  );
}

export default App;