import React, { useState, useContext } from 'react';
import { ScrollView, Text, TextInput, Picker, View, Button, Image, TouchableHighlight, FlatList, SafeAreaView } from 'react-native';
import style from './style';
export default function Home(props) {

  const handleSignOut = () => {
    props.navigation.navigate('Login')
  }

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
    <Text>
      Homescreen
    </Text>
    <View style={style.buttonstyle}>
        <Button title='Sign Out' onPress={handleSignOut}  color="#ff5851" />
    </View>
  </View>

  )
}

