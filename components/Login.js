// Login.js
import React, {useEffect, useState, useContext } from 'react';
import { ScrollView, Text, TextInput, View, Button, Image, TouchableHighlight } from 'react-native';
import styles from './style';
import style from './style';


// import style from './style';
import Icon  from 'react-native-vector-icons/FontAwesome5';


function Login(props) {

    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [username, setUsername] = useState('');
    const [Password, setPassword] = useState('');

    // const {signIn} = useContext(AuthContext);

    const handleLogin = () => {
        console.log(username , Password);
        console.log(username ==='admin'&& Password === '12345'  );
        if(username ==='admin'&& Password === '12345'){
            
                props.navigation.navigate('Home')
        } else {
            alert("Incorrect credentials");
        }
       
    }

    return (
        // <ScrollView style={style.height}>

            <View style={style.container}>
                
                <View>
                    <Image
                         source={require('../assets/logo.png')}
                         style={style.ImageStyle}
                    />
                </View>
                {/* <Text style={style.appName}>Azad</Text> */}
                <Text style={style.subHeading}>Together, We set freedom</Text>
                <View style={styles.inputSection}>
                    <View style={styles.SectionStyle}>
                        <Icon 
                        name='phone'
                        style={{ paddingVertical: 5, paddingHorizontal: 8, fontSize: 16 }}
                        
                        />

                        <TextInput style={styles.textInput}
                            autoCapitalize="none"
                            placeholder="Username"
                            placeholderTextColor='#cccccc'
                            onChangeText={(input) => setUsername(input)}
                        />
                    </View>
                </View>
                <View style={styles.inputSection}>

                    <View style={styles.SectionStyle}>
                    <Icon 
                        name='lock'
                        style={{ paddingVertical: 5, paddingHorizontal: 8, fontSize: 16 }}
                        
                        />
                        <TextInput style={styles.textInput}
                            autoCapitalize="none"
                            placeholder="Password"
                            placeholderTextColor='#cccccc'
                            secureTextEntry={true}
                            onChangeText={(input) => setPassword(input)}
                        />
                    </View>
                </View>
                <View style={styles.buttonStyles}>
                    <Button title='Login' onPress={handleLogin} color="#ff5851" />
                </View>
            </View>
            
        // </ScrollView>
    )
}



export default Login;