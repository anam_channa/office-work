// Login.js
import React, {useEffect, useState } from 'react';
import { ScrollView, Text, TextInput, View, Button, Image, TouchableHighlight } from 'react-native';
import styles from './style';

import style from './style';
import Icon  from 'react-native-vector-icons/FontAwesome5';


function Signup(props) {

    
    const handleSignup = () => {
        props.navigation.navigate('Login')
    }

    return (
        <ScrollView>

            <View style={style.container}>
                
                {/* <View>
                    <Image
                         source={require('./assets/111.jpg')}
                         style={style.ImageStyle}
                    />
                </View> */}
                <Text style={style.appName}>Azad</Text>
                <Text style={style.subHeading}>Single app for all your shipping needs.</Text>
                <View style={styles.inputSection}>

                    <View style={styles.SectionStyle}>
                        <Icon 
                        name='phone'
                        style={{ paddingVertical: 5, paddingHorizontal: 8, fontSize: 16 }}
                        
                        />

                        <TextInput style={styles.textInput}
                            autoCapitalize="none"
                            placeholder="Username"
                            placeholderTextColor='#cccccc'
                        />
                    </View>
                </View>
                <View style={styles.inputSection}>

                    <View style={styles.SectionStyle}>
                    <Icon 
                        name='lock'
                        style={{ paddingVertical: 5, paddingHorizontal: 8, fontSize: 16 }}
                        
                        />
                        <TextInput style={styles.textInput}
                            autoCapitalize="none"
                            placeholder="Password"
                            placeholderTextColor='#cccccc'
                        />
                    </View>
                </View>
                <View style={styles.buttonStyles}>
                    <Button title='Sign Up' onPress={handleSignup} color="#ff5851" />
                </View>
            </View>
            
        </ScrollView>
    )
}



export default Signup;