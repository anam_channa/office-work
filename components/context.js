import React, { createContext } from 'react';
import AppReducer from './AppReducer';

const initialState = {
    userInfo : [
        {username : 'admin' , password : '12345'}
    ]
}

export const GlobalContext = createContext(initialState);

// Provider Component
export const GlobalProvider = ({children}) => {
    const [state, dispatch] = useReducer(AppReducer, initialState);
    return (
        <GlobalContext.Provider value={{userInfo : state.userInfo }}>
            {children}
        </GlobalContext.Provider>
    )
}