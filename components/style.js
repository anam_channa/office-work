import {StyleSheet} from 'react-native'
export default StyleSheet.create({
    login: {
      fontSize: 26,
      fontWeight: "bold",
  
  
    },
    
    logoSection: {
      marginBottom: 25
    },
    label: {
      fontSize: 15,
      color: '#a7a7a7',
      fontWeight: 'bold',
      paddingLeft: 5
    },
    inputSection: {
      marginTop: 25,
      width : '80%'
    },
    height : {
      // height:"100%",
      backgroundColor:'red'
    },
    SectionStyle: {
      flexDirection: 'row',
      // justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#fff',
      borderWidth: 1,
      borderColor: '#dcdcdc',
      height: 40,
    },

    mainContainer : {paddingHorizontal:4},
    container: {
      flex: 1,
      display:'flex',
      
      height:"100%",
      justifyContent : "center",
      alignItems : 'center',
      backgroundColor: "rgba(0,0,0,0.7)",
      // paddingBottom:"100%"
    //   margin: 25,
    //   marginTop: 40
  
    },
    summaryCont : {
      margin :8,
      marginTop : 15,
      borderWidth : 1,
      borderColor : '#e9e9e9',
      borderTopStartRadius : 10,
      borderTopEndRadius : 10,
      
      elevation : 0.8
    },
    tabOneContainer : {
      display :'flex',
      flex: 1,
      // justifyContent:'center',
      alignItems:'center',
      
      flexDirection:'column',
      marginBottom:170
      // backgroundColor:'red'
    },
    headingSection : {
      marginBottom :15
    },
    heading: {
      fontSize: 20,
      fontWeight: 'bold',
      color: '#000000'
    },
    subHeading: {
      fontSize: 14,
      fontWeight: 'normal',
      color: '#cccccc',
      marginTop: 10
    },
    textInput: {
      height: 39,
      width: '90%',
      textAlignVertical: 'center'
  
  
    },
    ImageStyle: {
    //   padding: 5,
      // marginTop: 100,
      height: 190,
      width: 260,
    //   resizeMode: 'stretch',
      alignItems: 'center',
    },
    imgStyle: {
      //   padding: 5,
        marginTop : 130,
        height: 170,
        width: 300,
      //   resizeMode: 'stretch',
        alignItems: 'center',
      },
    buttonStyles: {
      marginVertical: 25,
      width : '80%'
    },
    sumBtnStyle : {
      paddingHorizontal: 14,
      marginTop :10,
      width : '100%'
    },
    buttonStyle : {
      paddingHorizontal: 15,
      width : '100%',
      marginBottom:10,
      marginTop:10
    },
    button : {
      textAlign:'center',
      paddingVertical: 10,
      // backgroundColor : '#4a3973',
      backgroundColor : '#ff5851',
      color:'white'
    },
    errorStyles: {
      color: 'darkred',
      marginTop: 10
    },
  
    SocialLoginSection: {
      display: 'flex',
  
  
    },
  
    line: {
      borderBottomColor: '#cccccc',
      marginTop: 30,
      borderBottomWidth: 1,
      position: 'relative',
  
    },
    orText: {
      color: '#cccccc',
      position: 'absolute',
      backgroundColor: 'white',
      width: 35,
      height: 25,
      textAlignVertical: 'center',
      textAlign: 'center',
      top: 18,
      left: '45%'
    },
    socialBtnSection: {
      marginTop: 25,
      display: 'flex',
      justifyContent: 'center',
      alignItems: "center"
  
    },
    circle: {
      width: 40,
      height: 40,
      backgroundColor: 'white',
      borderWidth: 0.5,
      borderColor: "#dcdcdc",
      borderRadius: 50,
      marginRight: 20,
      marginTop: 25,
      display: 'flex',
      justifyContent: 'center',
      flexDirection: 'column',
      alignItems: 'center'
  
    },
    socialBtns: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
  
  
  
    },
    socialImage: {
      width: 23,
      height: 23
    },
    socialImagem: {
      width: 19,
      height: 19
    },

    servicesImage: {
      width: 45,
      height: 45
    },

    // New Styles

    appName : {
        fontWeight : "bold",
        fontSize : 30,
        marginTop: 30,
        
        
    },
    itemStyle: {
        fontSize: 15,
        height: 75,
        width : '80%',
        color: 'black',
        textAlign: 'center',
        fontWeight: 'bold',
        justifyContent : 'center',
        alignItems : 'center'
        
      },
    picker: {
        width: '100%',
        justifyContent : 'center',
        alignSelf : 'center'
        
      },

      item: {
        padding: 10,
        fontSize: 18,
        height: 44,
        borderWidth : 1,
        borderColor : '#ebecf0'
      },
      servicecontainer : {
           flex: 1,
           margin:10
   
      },
      header : {
        display: "flex",
        paddingHorizontal : 15,
        paddingVertical : 10,
        // height : 50,
        // backgroundColor : "#65bbff",
      },
      row : {
        flex : 1,
        display : 'flex',
        flexDirection : 'row',
        paddingVertical: 15,
        
      },
  
      rowItem : {
        backgroundColor : "#f8f9fa",
        borderRadius : 10,
        height : '100%',
        paddingVertical : 25,
        // flexGrow : 1,
        width : "45%",
        margin :8,
        justifyContent : "center",
        alignItems:"center"
      },
      serviceText : {
        fontWeight : 'bold',
        paddingTop : 4,
        textAlign : 'center',
        width : "100%"
        

      },

      innerItem : {
        display : 'flex',justifyContent : 'center',alignItems:'center'
      },
      ScrollView : {
        flex : 1,
       paddingVertical:100
      },

      serviceDets : {
        display : "flex",
        padding : 15
        // justifyContent : 'center',
        // alignItems : 'center',
        
        
      },
      
      detsText : {
        paddingVertical : 8,
        
      },
      serviceDetsBg : {
        // borderRadius:50,
        paddingHorizontal :18,
        paddingTop : 13,
        borderTopStartRadius : 30,
        borderTopEndRadius : 30,
        // paddingVertical : 65
        flex:1,
        backgroundColor : 'white',
        marginTop : -95
        
      },
      pickupBox : {
        zIndex :12,
        elevation:5,
        // borderRadius:5,
        marginHorizontal :20,
        paddingVertical:10,
        paddingHorizontal:10,
       
        width:'91%',
        top:70,
        position:'absolute',
        backgroundColor:'white'

      },
      confirmBtnCont : {
        zIndex :12,
        elevation:10,
        borderRadius:5,
        marginHorizontal :20,
        paddingVertical:5,
       
        width:'91%',
        top:605,
        position:'absolute',
        backgroundColor:'#ff5851'
      },
      detailsCont : {
        display : 'flex',
        backgroundColor : 'white',
        borderTopStartRadius : 30,
        borderTopEndRadius : 30,
        elevation : 10,
        height : 650,
        marginTop : -45,
        justifyContent : 'center',
        alignItems : 'center',
        
      },
      headingDets: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
      },
      detailsTop : {
        paddingHorizontal:4
      },
      ScrollView : {
        flex : 1
      },
      pickup : {
        display : "flex",
        flexDirection : 'row',
        backgroundColor : 'white',
        borderRadius : 10,
        elevation : 5,
        height : 60,
        width : '90%',
        position : 'absolute',
        marginVertical :15,
        marginHorizontal : 20,
        top : 50,
      },
      list : {
        // borderWidth:1,
        // borderColor:'#f1f1f1',        
        borderRadius:20,        
        // marginVertical :25,
        flexDirection:'row',        
        paddingVertical : 15,
        paddingHorizontal : 8,        
        zIndex :12,
        display:'flex',
      },
      listTwo : {
        // borderWidth:1,
        // borderColor:'#f1f1f1',        
        borderRadius:20,        
        // marginVertical :25,
        flexDirection:'row',        
        paddingTop : 25,
        paddingHorizontal : 15,        
        zIndex :12,
        display:'flex',
      },
      paymentContainer: {
        borderWidth:1,
        borderColor:'#f1f1f1',        
        borderRadius:20,        
        
        flexDirection:'row',        
        paddingVertical : 25,
        paddingHorizontal : 15,        
        display:'flex',
      },
      pickup : {
        display:'flex',
        flexDirection:'row',
        flex : 1,
        // borderBottomWidth :1,
       
        borderColor:'#f1f1f1',
        paddingVertical : 19,
        
        // padding : 15 

      },
      dropoff : {
        display:'flex',
        flexDirection:'row',
        flex : 1,
        // borderBottomWidth :1,
       
        // borderColor:'#f1f1f1',
        paddingVertical : 19,
        
        
        // padding : 15 

      },
      map : {
        height:'100%',
        width:'100%'
      },

      // Details Screen Styles
      detailCont  : {
        margin :8,
        marginTop : 15,
        borderWidth : 1,
        borderColor : '#e9e9e9',
        borderTopStartRadius : 10,
        borderTopEndRadius : 10,
        elevation : 0.8
        
        // padding:25
      },

      detailHeader : {
        backgroundColor : "#f8f9fa",
        borderBottomColor : '#e9e9e9',
        borderBottomWidth:2,
        paddingHorizontal : 20,
        paddingVertical : 25,
        marginBottom:15
      },
      detailFooter : {
        borderTopColor : '#e9e9e9',
        borderTopWidth:2,
        // paddingHorizontal : 20,
        // paddingVertical : 25,
        // marginBottom:15
      },
      
      detHeading : {
        fontSize:16,
        fontWeight:'bold'
      },
      footerHeading : {
        fontSize:16,
        backgroundColor : "#f8f9fa",
        fontWeight:'bold',
        borderBottomWidth : 2,
        borderBottomColor : '#e9e9e9',
        paddingVertical : 25,
        // paddingBottom :25,
        paddingHorizontal : 20,
        marginBottom:10
      },

      detSubHeading : {
        color:'#afafb0',
        width:'90%',
        marginTop : 8
        
      },

      sumSubHeading : {
        color:'#afafb0',
        marginTop :8,
        marginBottom :15,
        fontSize:13.5
      },

      detailItems : {
        // paddingTop:8,
        borderBottomColor : '#e9e9e9',
        borderBottomWidth:1,
        // marginHorizontal : ,
        marginVertical : 8,
        // paddingTop:10,
        // paddingBottom : 10,
        flexDirection : 'row'

      },
      summaryItems : {
         // paddingTop:8,
        //  borderBottomColor : '#e9e9e9',
        //  borderBottomWidth:1,
         paddingHorizontal : 15,
        //  paddingVertical : 15,
        //  marginVertical : 10,
         // paddingTop:10,
        //  paddingBottom : 12,
         flexDirection : 'row',
         backgroundColor:'#f8f9fa'
 
      },
      detailItemLast : {
        // paddingTop:8,
        // borderBottomColor : '#e9e9e9',
        // borderBottomWidth:1,
        // marginHorizontal : ,
        marginVertical : 10,
        // paddingTop:10,
        // paddingBottom : 10,
        flexDirection : 'row'

      },

      paymentCont : {
        flexDirection : 'row',
        marginVertical : 10,
        // paddingHorizontal : 10,
        borderBottomWidth : 1,
        borderBottomColor : '#e9e9e9',
        paddingBottom : 20
        // paddingVertical :10
      },
      paymentContLast : {
        flexDirection : 'row',
        // marginVertical : 10,
        // paddingHorizontal : 20,
        // borderBottomWidth : 1,
        // borderBottomColor : '#e9e9e9',
        paddingBottom : 20,
        paddingTop : 10
        // paddingVertical :10
      },

      bold : {
        fontWeight : 'bold',
        
        // color:'#c4c4c4',
        fontSize :13
      },
      icon : {
        fontSize : 12,
        marginTop:2,
        
      },
      iconSelected : {
        color:'#ff5851',
        display:'none'
      },

      icon_b : {marginTop:30,fontSize:17,marginLeft:-25},
      column : { flexGrow: 2, paddingLeft: 20, flexDirection: 'column' },
      
      summaryColumn : { flexGrow: 2, flexDirection: 'column' },
      InputStyles : {
        marginLeft :-4,
        marginTop:-5
      },
      servicePicker : {
        marginTop:-15,
        marginLeft:-10
      },
      servicesCont : {
        padding : 15,
        display :'flex',
        flex : 1
      }
  
  })

